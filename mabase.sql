-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 23 nov. 2018 à 13:59
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mabase`
--

-- --------------------------------------------------------

--
-- Structure de la table `bateau`
--

CREATE TABLE `bateau` (
  `id_bateau` int(22) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `origine` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `id_proprietaire` int(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file_url` varchar(255) NOT NULL,
  `nom_img` varchar(255) NOT NULL,
  `image_url` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `bateau`
--

INSERT INTO `bateau` (`id_bateau`, `nom`, `origine`, `type`, `id_proprietaire`, `name`, `file_url`, `nom_img`, `image_url`) VALUES
(12, 'BAYE', 'senegal', 'NAVIRE', 221, 'Exam_C_Embarque_FI_2017_2018.pdf', 'files/Exam_C_Embarque_FI_2017_2018.pdf', '', ''),
(13, 'royer', 'france', 'croisiÃ¨re', 33, 'sujet.pdf', 'files/95341_0_990x742.jpg', '', ''),
(9, 'Moi', 'Lui', 'Toi', 10, 'Exam_C_Embarque_FA_2016_2017_solutions.pdf', 'files/Exam_C_Embarque_FA_2016_2017_solutions.pdf', '', ''),
(10, 'Moi', 'Lui', 'Toi', 10, 'Exam_C_Embarque_FA_2016_2017_solutions.pdf', 'files/Exam_C_Embarque_FA_2016_2017_solutions.pdf', '', ''),
(11, 'Moi', 'Lui', 'Toi', 10, 'Exam_C_Embarque_FA_2016_2017_solutions.pdf', 'files/Exam_C_Embarque_FA_2016_2017_solutions.pdf', '', ''),
(14, 'maxime', 'france', 'voiliÃ©', 34, 'ulrich fosso  job.pdf', 'files/ulrich fosso  job.pdf', '', ''),
(15, 'fhgf', 'teftg', 'hhuhiuygh', 36, 'Apprentissage _auto Ã©valuation_Comportement_aptitudeV2.pdf', 'files/Apprentissage _auto Ã©valuation_Comportement_aptitudeV2.pdf', '', ''),
(16, 'fhgf', 'teftg', 'hhuhiuygh', 36, 'th.jpg', 'files/Apprentissage _auto Ã©valuation_Comportement_aptitudeV2.pdf', '', 'fileimage/'),
(17, 'fhgf', 'teftg', 'hhuhiuygh', 36, 'th.jpg', 'files/Apprentissage _auto Ã©valuation_Comportement_aptitudeV2.pdf', '', 'fileimage/th.jpg'),
(18, 'fhgf', 'teftg', 'hhuhiuygh', 36, 'Apprentissage _auto Ã©valuation_Comportement_aptitudeV2.pdf', 'files/Apprentissage _auto Ã©valuation_Comportement_aptitudeV2.pdf', '', 'fichimage/th.jpg'),
(19, 'foster', 'kmer', 'nav', 237, 'CONTRAT DE SCOLARITE 2016-2017.pdf', 'files/CONTRAT DE SCOLARITE 2016-2017.pdf', '3D Wallpaper Dans 3D HD Desktop Wallpapers Romania.jpg', 'fichimage/3D Wallpaper Dans 3D HD Desktop Wallpapers Romania.jpg'),
(20, 'maxime', 'france', 'pirogue', 36, 'dm1-c.pdf', 'files/dm1-c.pdf', '3d_space_029.jpg', 'fichimage/3d_space_029.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `personnes`
--

CREATE TABLE `personnes` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `dateDeNaissance` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT 'inscrit'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `personnes`
--

INSERT INTO `personnes` (`id`, `nom`, `prenom`, `dateDeNaissance`, `email`, `mdp`, `role`) VALUES
(1, 'F', 'V', '2010-10-12', 'dgvnsd@edzsdf', '1245', ''),
(2, 'on', 'nkl', '54154698', 'nbvpebf', 'iowifoff', ''),
(3, 'nouboue', 'cyrille', '2000-12-12', 'cyrille@yahoo.fr', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', ''),
(4, 'nouboue', 'cyrille', '2000-12-12', 'cyrille@yahoo.fr', 'fc7a734dba518f032608dfeb04f4eeb79f025aa7', ''),
(6, 'noubs', 'cril', '2018-11-04', 'fgggh@gg', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'visiteur'),
(7, 'nouboue', 'cyrille', '2018-11-04', 'cynou@yahoo.fr', '23b23be9da2519c88f11c084310bcc0bf14417f8', 'visiteur'),
(8, 'gueye', 'satou', '2018-11-02', 'sa@gmail.com', 'fc1200c7a7aa52109d762a9f005b149abef01479', 'inscrit');

-- --------------------------------------------------------

--
-- Structure de la table `propriétairebateaux`
--

CREATE TABLE `propriétairebateaux` (
  `id` int(22) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(2555) NOT NULL,
  `dateDeNaissance` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bateau`
--
ALTER TABLE `bateau`
  ADD PRIMARY KEY (`id_bateau`);

--
-- Index pour la table `personnes`
--
ALTER TABLE `personnes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `propriétairebateaux`
--
ALTER TABLE `propriétairebateaux`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `bateau`
--
ALTER TABLE `bateau`
  MODIFY `id_bateau` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `personnes`
--
ALTER TABLE `personnes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `propriétairebateaux`
--
ALTER TABLE `propriétairebateaux`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
