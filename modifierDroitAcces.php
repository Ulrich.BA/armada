<?php  session_start();?>
<!DOCTYPE html>

    <head>
    	<html>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="longin1css.css">
        <meta charset="utf-8" />
        <title>Modification des rôle</title>
    </head>

    <body>
    <div class="container">
    <?php if($_SESSION['role'] == "administrateur"){?>
        <div class="row">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            
            <div class="container-fluid">
              <ul class="nav navbar-nav">
                <li class="active"> <a href="index.php">Accueil</a> </li>
                <li> <a href="unlogin.php"><span class="glyphicon glyphicon-user"></span>Déconnexion</a> </li>
                <li> <a href="infoDetaillees.php">informations détaillées </a></li>
                <li> <a href="modifierole.php">consulter les droits d'accès </a></li>
                <li> <a href="modifierDroitAcces.php">modifier les droits d'accès </a></li>
              </ul>
              <form class="navbar-form navbar-right inline-form">
                <div class="form-group">
                  <input type="search" class="input-sm form-control" placeholder="Recherche">
                  <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                </div>
              </form>
            </div>
        </nav><br> <br>
            <div class="col-md-4 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-lock"></span> Modifier le roles des personnes</div>
                        <div class="panel-body">
                            <!-- connexion à la base de donnée -->
                            <?php include 'database.php';?>
                            
                            <form class="needs-validation" action="traitementmodifierole.php" method="POST" novalidate>
                                <div class="form-group">
                                    <label for="id">Identifiant: *</label>
                                    <input type="number" name="id" class="form-control" id="id" min='1' max=$personnes['id'] required placeholder="Veuillez entrer l'id de la personne">
                                    <div class="invalid-feedback">
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="role">Role : </label>
                                        <select name ="role" id="role" class="form-control">
                                            <option>inscrit</option>
                                            <option>Propriétaire de bateaux</option>
                                            <option>administrateur</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
									<div class="col-lg-2">
									</div>
									<div class="col">
										<button class="btn btn-primary" type="submit">Confirmer</button>
									</div>
								</div>
								</br>
                            </form>
                        </div>
                    
                    </div>
                </div>
            </div>
            <?php }else{ ?>
        <div class="col-md-4 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span> Attention</div>
                    <div class="panel-body">
                        <p> Vous n'avez pas acces a cette page </p>
                        <?php
                              if( $_SESSION['role'] == "inscrit" ){
                                ?> <a href="inscrit.php">Accueil</a><?php
                              } 

                              if( $_SESSION['role'] == "Propriétaire de bateaux" ){
                                ?> <a href="proprietaire.php">Accueil</a><?php
                              }

                              if( $_SESSION['role'] != "Propriétaire de bateaux" && $_SESSION['role'] != "inscrit" ){
                                ?> <p><a href="index.php">Accueil</a></p><?php
                              }} ?>

                    </div>
                
                </div>
              </div>
        </div>
    </body>
</html>