<?php  session_start();?>
<!DOCTYPE html>

    <head>
    	<html>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="longin1css.css">
        <meta charset="utf-8" />
        <title>Liste des bateaux</title>
    </head>

    <body>
    <div class="container">
    <?php
                              if( $_SESSION['role'] == "inscrit" ){?>
                                <nav class="navbar navbar-inverse navbar-fixed-top">
            
                                <div class="container-fluid">
                                  <ul class="nav navbar-nav">
                                      <li class="active"> <a href="index.php">Accueil</a> </li>
                                      <li> <a href="info-bateau.php">info sur les bateaux</a> </li>
                                      <li> <a href="infoDetaillees.php">informations détaillées </a></li>
                                      <li> <a href="unlogin.php"><span class="glyphicon glyphicon-user"></span>Déconnexion</a> </li>
                                      
                                  </ul>
                                  <form class="navbar-form navbar-right inline-form">
                                    <div class="form-group">
                                      <input type="search" class="input-sm form-control" placeholder="Recherche">
                                      <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                                    </div>
                                  </form>
                                </div>
                        </nav>
                        </div>
                        
                    <br><br>
                    <?php } 

                              if( $_SESSION['role'] == "Propriétaire de bateaux" ){?>
                                <nav class="navbar navbar-inverse navbar-fixed-top">
            
                                <div class="container-fluid">
                                <ul class="nav navbar-nav">
                                    <li class="active"> <a href="index.php">Accueil</a> </li>
                                    
                                    <li> <a href="info-bateau.php">info sur les bateaux</a> </li>
                                    <li> <a href="infoDetaillees.php">informations détaillées </a></li>
                                    <li> <a href="formulaireBateau.php">editer un bateau </a></li>
                                    <li> <a href="unlogin.php"><span class="glyphicon glyphicon-user"></span>Déconnexion</a> </li>
                                </ul>
                                <form class="navbar-form navbar-right inline-form">
                                    <div class="form-group">
                                    <input type="search" class="input-sm form-control" placeholder="Recherche">
                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                                    </div>
                                </form>
                                </div>
                        </nav><br><br>
                        <?php
                              }

                              if( $_SESSION['role'] == "administrateur" ){?>
                                <nav class="navbar navbar-inverse navbar-fixed-top">
            
                                    <div class="container-fluid">
                                    <ul class="nav navbar-nav">
                                        <li class="active"> <a href="index.php">Accueil</a> </li>
                                        
                                        <li> <a href="info-bateau.php">info sur les bateaux</a> </li>
                                        <li> <a href="infoDetaillees.php">informations détaillées </a></li>
                                        <li> <a href="modifierole.php">consulter les droits d'accès </a></li>
                                        <li> <a href="modifierDroitAcces.php">modifier les droits d'accès </a></li>
                                        <li> <a href="unlogin.php"><span class="glyphicon glyphicon-user"></span>Déconnexion</a> </li>
                                    </ul>
                                    <form class="navbar-form navbar-right inline-form">
                                        <div class="form-group">
                                        <input type="search" class="input-sm form-control" placeholder="Recherche">
                                        <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                                        </div>
                                    </form>
                                    </div>
                                </nav> <br><br> <?php
                              }
                              if(!$_SESSION['role']){?>
                                <nav class="navbar navbar-inverse navbar-fixed-top"> 
                                <div class="container-fluid">
                                  <ul class="nav navbar-nav">
                                    <li class="active"> <a href="index.php">Accueil</a> </li>
                                    <li> <a href="formulaire.php">Inscription</a> </li>
                                    <li> <a href="longin1.php">Connection</a> </li>
                                    <li> <a href="info-bateau.php">info sur les bateaux</a> </li>
                                  </ul>
                                  <form class="navbar-form navbar-right inline-form">
                                    <div class="form-group">
                                      <input type="search" class="input-sm form-control" placeholder="Recherche">
                                      <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                                    </div>
                                  </form>
                                </div>
                            </nav> <br><br>
                             <?php } ?>
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-lock"></span> Liste de Bateaux </div>
                        <div class="panel-body">
                            <!-- connexion à la base de donnée -->
                            <?php include 'database.php';?>
                            <?php
                                global $db;

                                $res = $db->query("SELECT * FROM bateau");
                                while($bateau = $res->fetch()){?>
                                    <br/>
                                        <p>
                                            <?php echo "<div>" .'<img src="'.$bateau['image_url'].'" width="110px"> ' . "Le bateau n°" . $bateau['id_bateau'] . " Il a pour nom " . $bateau['nom'] . " et est originaire de: " . $bateau['origine'] . "</div>";?>
                                            
                                        <p>
                                    

                            <?php } ?>
                            
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
