
<?php session_start(); ?>
<!DOCTYPE html>

    <head>
    	<html>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="longin1css.css">
        <meta charset="utf-8" />
        <title>formulaire des bateaux</title>
    </head>

    <body>
       
        <div class="container">
        <!-- Menu de navigation -->
        <?php if($_SESSION['role'] == "Propriétaire de bateaux"){?>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            
            <div class="container-fluid">
              <ul class="nav navbar-nav">
                <li class="active"> <a href="index.php">Accueil</a> </li>
                <li> <a href="unlogin.php"><span class="glyphicon glyphicon-user"></span>Déconnexion</a> </li>
                  <li> <a href="infoDetaillees.php">informations détaillées </a></li>
                  <li> <a href="formulaireBateau.php">editer un bateau </a></li>
              </ul>
              <form class="navbar-form navbar-right inline-form">
                <div class="form-group">
                  <input type="search" class="input-sm form-control" placeholder="Recherche">
                  <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                </div>
              </form>
            </div>
      </nav><br><br><br>
      
        <div class="row">
        
            <div class="col-md-4 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-lock"></span> formulaire</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" class="needs-validation" action="traitementFormBateau.php" method="POST" enctype="multipart/form-data" novalidate>
                        <div class="form-group">
                            <label for="lastName" class="col-sm-3 control-label">Nom: </label>
                            <div class="col-sm-9">
                                <input type="text" name="nom" class="form-control" id="lastName" placeholder="nom" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-3 control-label">type: </label>
                            <div class="col-sm-9">
                                <input type="text" name="type" class="form-control" id="type" placeholder="type de bateaux" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="origine" class="col-sm-3 control-label">origine: </label>
                            <div class="col-sm-9">
                                <input type="text" name="origine" class="form-control" id="origine" placeholder="origine du bateaux" required>
                            </div>
                        </div>    
                            <div class="form-group">
                                <label for="id_propietaire" class="col-sm-3 control-label">identifiant: </label>
                                <div class="col-sm-9">
                                    <input type="text" name="id_propietaire" class="form-control" id="id_propietaire" placeholder="identifiant" required>
                                </div>
                            </div>

                            <label for="mon_fichier">télechager un fichier PDF:</label><br />
                             <input type="hidden" name="MAX_FILE_SIZE" value="52428880" />
                             <input type="file" name="fichier" id="fichier" /><br />
                             <label for="titre">telecharger une image :</label><br />
                             <input type="file" name="image" id="mon_image" /><br />
                                <br />               
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm">
                                    valider</button>
                                     <button type="reset" class="btn btn-default btn-sm">
                                    Reset</button>
                            </div>
                        </div>
                        </form>

                    </div>
                    <div class="panel-footer">
                        info sur les bateaux? <a href="formulaire.php">cliquez ici</a></div>
                </div>
            </div>
        </div>
        <?php } else{ ?>

<div class="col-md-4 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-lock"></span> Attention</div>
                <div class="panel-body">
                    <p> Vous n'avez pas acces a cette page </p>
                    <?php
                          if( $_SESSION['role'] == "administrateur" ){
                            ?> <a href="admin.php">Accueil</a><?php
                          } 

                          if( $_SESSION['role'] == "inscrit" ){
                            ?> <a href="inscrit.php">Accueil</a><?php
                          }

                          if( $_SESSION['role'] != "inscrit" && $_SESSION['role'] != "administrateur" ){
                            ?> <p><a href="index.php">Accueil</a></p><?php
                          }} ?>

                </div>
            
            </div>
          </div>
    </div>
   
     
      
            
        </body>
    </html>