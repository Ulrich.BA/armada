<?php session_start(); ?>
<!DOCTYPE html>

    <head>
    	<html>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="longin1css.css">
        <meta charset="utf-8" />
        <title>page des inscrits</title>
    </head>

    <body>
    <div class="container">
         
    <div class="row">
    <?php if($_SESSION['role'] == "inscrit"){?>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            
            <div class="container-fluid">
              <ul class="nav navbar-nav">
                  <li class="active"> <a href="inscit.php">Accueil</a> </li>
                  <li> <a href="info-bateau.php">info sur les bateaux</a> </li>
                  <li> <a href="infoDetaillees.php">informations détaillées </a></li>
                  <li> <a href="unlogin.php"><span class="glyphicon glyphicon-user"></span>Déconnexion</a> </li>
                  
              </ul>
              <form class="navbar-form navbar-right inline-form">
                <div class="form-group">
                  <input type="search" class="input-sm form-control" placeholder="Recherche">
                  <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
                </div>
              </form>
            </div>
    </nav>
    </div>
    
<br><br><br>

        <div class="row">
        
        <div class="col-md-1"></div>

         
          <div class="col-md-5">
            
           <div class="row">
            <div class="col-md-17 offset-3 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-pencil"></span> <h1><img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/Vignette_Armada_Rouen_2019.png" alt="" height="150px" />DISCOURS</h1> </div>
           <blockquote>
            <p><span class="glyphicon glyphicon-hand-right"></span> Emmanuel MACRON</p><br>
Monsieur le Président,<br>
L’Armada 2019 marquera la 7ème édition de la réunion des plus grands et plus beaux voiliers du monde sur les quais de Rouen.
C’est en juillet 1989 que naît ce rendez-vous international avec la participation de navigateurs venus des quatre coins du globe. Sous votre impulsion, il draine rapidement des millions de visiteurs et de passionnés.<br>
Aujourd’hui installé dans le paysage institutionnel rouennais, cet évènement est devenu l’un des plus importants rassemblements maritimes mondiaux et remporte un immense succès populaire.<br>
Aussi, je me réjouis de l’organisation de cette nouvelle édition qui marque également le 30ème anniversaire de l’Armada, et tiens à saluer l’engagement continu de votre association en faveur du rayonnement et de la vitalité de ce territoire et de ses habitants.<br>
Soyez assuré de tout mon soutien et de mes vœux sincères pour que cet anniversaire soir une grande et belle réussite.<br>
Je vous prie de croire, Monsieur de Président, à l’assurance de mes sentiments les meilleurs.<br>
Bien à vous,<br>
<strong>Emmanuel MACRON</strong><br>

<!-- <div class="row">
            <div class="col-md-4 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-lock"></span> <h1>PAROLE A</h1> </div>
                        <blockquote> -->
<br><p><span class="glyphicon glyphicon-hand-right"></span> Amiral Christophe Prazuck</p>
Monsieur,<br>
J’ai été très heureux de vous rencontrer et suis très sensible à l’éclat particulier que vous comptez donner à l’Armada 2019, qui rassemblera à Rouen du 06 au 16 juin 2019, de nombreux voiliers et bâtiments de guerre de plusieurs pays. Cette manifestation interviendra à l’occasion du soixante quinzièmes anniversaires du débarquement. Ce beau rassemblement qui marquera également les trente ans de votre association, mettre du sel et du rêve dans la vie des dizaines de millions de visiteurs attendus.<br>
Pour sa 7ème édition, l’Armada constituera un évènement grandiose autour des bateaux et de leurs équipages venus du monde entier. La marine nationale est heureuse de s’associer à cet évènement majeur de la ville de Rouen et du monde maritime. Elle vous apporte son soutien officiel et accepte bien volontiers de parrainer l’Armada 2019.<br>
Je souhaite que ce rassemblement soit, dans la lignée des précédentes manifestations, un grand succès populaire et maritime.<br>
Bien cordialement,<br>
<strong>Amiral Prazuck</strong><br><br>

<p><span class="glyphicon glyphicon-hand-right"></span> Pascal Martin</p>
Trente années que l’Armada jette l’ancre en Seine-Maritime pour faire vivre à ses habitants et ses touristes l’un des beaux événements au monde dans le domaine maritime.<br> 
Premier partenaire de cette manifestation, le Département a toujours été soucieux de préserver le caractère populaire de celle-ci ainsi que  l’accès au plus grand nombre des navires amarrés aux quais de Rouen. C’est ainsi qu’en plus d’une contribution financière très importante, il apprête tout au long de cette nouvelle édition, un bateau-promenade prioritairement pour y accueillir les jeunes collégiens du territoire et une autre embarcation pour permettre au public à mobilité réduite et en situation de handicap de vivre au cœur de l’événement.<br> 
Parallèlement, il se mobilise avec les communes des bords de Seine pour animer les berges du fleuve et faciliter l’accès à la Grande Parade finale à tous ceux qui souhaitent saluer une dernière fois les marins du monde entier.<br> 
Bonne Armada 2019 à tous<br> 
<strong>Pascal Martin</strong>, Président du Conseil Départemental de la Seine-Maritime<br><br>

<p><span class="glyphicon glyphicon-hand-right"></span> Frédéric SANCHEZ</p>
C’est avec bonheur que nous verrons revenir l’Armada en juin 2019. A l’occasion des trente ans de ce grand rendez-vous populaire au succès jamais démenti, qui contribue édition après édition au rayonnement de notre territoire, comment ne pas mesurer combien Rouen, notre ville, notre métropole, s’est transformée ? Du centre historique rénové aux quais reconquis – couronnés par le Grand prix national du paysage -, de la cathédrale mise en lumière à nos nouveaux équipements culturels, la Métropole est à l’ouvrage afin que nous puissions nous montrer sous notre meilleur visage : celui d’un territoire en train de se réinventer, à la fois attaché à la mise en valeur de son patrimoine exceptionnel et résolument tourné vers l’avenir, autant attaché à son identité, à sa singularité, qu’accueillant et ouvert sur le monde. Autour de nos paysages remarquables et de la Seine, le fil bleu de notre territoire, aussi bien urbaine, industrielle et portuaire que naturelle, nous allons faire de l’Armada 2019 un moment de fierté collective, de convivialité et de partage.<br>
 
<strong>Frederic SANCHEZ</strong>, Président de la METROPOLE ROUEN NORMANDIE<br><br>

<p><span class="glyphicon glyphicon-hand-right"></span> Yvon ROBERT</p>
Chère Madame, Cher Monsieur,<br>
Amoureux de la mer et des beaux gréements,<br>
Rouen est connue dans le monde entier pour son patrimoine exceptionnel, ses figures historiques comme Jeanne d’Arc, Flaubert ou Monet, mais aussi pour l’Armada.<br>
L’Armada, ce sont les plus beaux voiliers du monde qui remontent la Seine jusqu’aux quais à Rouen.<br>
L’Armada, c’est une Ville, un territoire en fête pendant 10 jours.<br>
L’Armada, ce sont des échanges et des rencontres uniques.<br>
L’Armada, c’est 30 ans d’histoire, d’anecdotes, de souvenirs inoubliables pour les Rouennais.<br>
L’Armada, c’est Rouen !<br>
La Ville de Rouen est une nouvelle fois heureuse d’accueillir le monde à l’occasion de la 7ème édition de l’Armada. Rendez-vous dans notre belle capitale normande du 6 au 16 juin 2019.<br>
Chaleureusement à vous,<br>
<strong>Yvon ROBERT</strong>, Maire de Rouen<br><br>

<p> <span class="glyphicon glyphicon-hand-right"></span> Nicolas OCCIS</p>
L’alliance HAROPA des trois ports de l’Axe Seine (Paris, Rouen, Le Havre) est heureuse et fière de s’associer une nouvelle fois à l’Armada de Rouen. Depuis « Les Voiles de la Liberté » en 1989, le Port de Rouen est un partenaire fidèle et toujours très impliqué dans la mise en place de cette grande fête populaire, organisée pour partie sur le domaine portuaire. Parmi les nombreux services du port impliqués, la Capitainerie du Port de Rouen mobilise ses moyens afin d’accueillir les plus beaux voiliers du monde dans des conditions de navigation et de sécurité optimales, et les services territoriaux interviennent en soutien des opérations terrestres. Grands voiliers, bâtiments des marines nationales, bateaux de promenade, marins et équipages animeront la ville et ses 7 km de quais pendant dix jours. Les millions de visiteurs attendus pour cette 7e édition profiteront des nouveaux aménagements réalisés le long du fleuve par la Ville de Rouen, la Métropole Rouen Normandie et le port. Le cœur de la ville se déplacera vers les rives de la Seine, qui ont connu d’heureuses transformations ces dernières années. Le Port de Rouen pavoisera lui aussi afin de rendre hommage aux marins venus du monde entier. À un an de l’événement, les équipes de bénévoles s’activent déjà sous la conduite de leur Président, Patrick Herr. Merci à tous pour ce bel événement gratuit qui réunit les Rouennais et les visiteurs autour de la Seine, et leur rappelle également la glorieuse histoire maritime et portuaire de notre cité, qui continue de se conjuguer, au présent et au futur.<br>
<strong>Nicolas Occis</strong> , Président de HAROPA, Directeur Général de HAROPA - Port de Rouen<br><br>
 

<p><span class="glyphicon glyphicon-hand-right"></span> Vincent LAUDAT</p>
Rouen accueillera en 2019 la 7e édition de l’Armada, le rassemblement des plus beaux bateaux du monde. Pendant 10 jours, la ville se transforme pour devenir le 1er port international, une vitrine pour les entreprises de notre territoire. Depuis sa 1ère édition il y a 30 ans, l’Armada constitue une formidable opportunité pour faire rayonner l’image de notre économie.<br> 
Plusieurs millions de visiteurs arpentent les quais de la Seine et participent à cette grande fête maritime. Les retombées économiques pour nos entreprises sont importantes. Commerçants et hôteliers mettent tout en œuvre pour que ce moment soit inoubliable et pour exposer au monde entier un cœur de ville dynamique et attractif.<br> 
C’est également une occasion unique pour rassembler les entrepreneurs de notre territoire afin d’échanger et de créer des rencontres d’affaires autour de ce bel évènement.<br>
La CCI Rouen Métropole est particulièrement enthousiaste à l’idée de participer de nouveau à cette manifestation de portée mondiale devenue l’évènement fédérateur de l’Axe Seine.<br>
<STRONG> Vincent LAUDAT</STRONG>, Président de la CCI ROUEN METROPOLE. 
           </blockquote></blockquote><br><br></div></div></div>
           
          </div>
            <div class="col-md-1">
            
          </div>

          <div class="col-md-4">

          <div class="row">
            <div class="col-md-17 offset-3 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-calendar"></span> <h1><img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/Vignette_Armada_Rouen_2019.png" alt="" height="150px" />Le programme</h1> </div>
            <!-- <h1>Le programme</h1> -->
            
            <blockquote>
            <br><br><span class="glyphicon glyphicon-arrow-right"> Mercredi 5 Juin<br>
En ouverture et en avant-première de l’Armada, la Seine s’anime … C’est la Grande pagaille. Les équipes doivent construire leur embarcation  et tentent de traverser la Seine sans couler à bord de leur OFNI : Objet Flottant Non Identifié.<br>
Assistez au lancement de la 7e édition de l’Armada en découvrant cette folle traversée riche en émotion, où les équipages rivalisent d’imagination et d’originalité.<br>
En clôture de cette journée, ne manquez pas la levée du Pont Flaubert, le soir entre 21h et minuit.<br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Jeudi 6 Juin<br>
De nouveau le Pont Flaubert se lève entre 5h et 6h du matin. Les voilà !<br>
C’est l’arrivée des navires ! Assistez à cette remontée de la Seine des plus beaux voiliers du monde. Saluez les marins venus de tous les horizons qui débarquent à Rouen, sur les quais, dans la ville.<br>
Pour les plus tardifs, la levée du pont Flaubert est également prévue le soir entre 21h et minuit.<br><br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Vendredi 7 Juin<br>
Soyez matinaux ! Le Pont Flaubert se lève entre 5h et 6h, une occasion de se promener sur les quais au petit matin ou alors il faut veiller tard car il se lève à nouveau entre 21h et minuit. Cette journée accueille le congrès du Mérite maritime, et en soirée, le magnifique feu d’artifice entre 23h et 23h30.<br>
 
Congrès du Mérite Maritime<br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Samedi 8 Juin<br>
Grand jour pour l’Armada ! L’inauguration officielle se déroule à 11h avec l’ensemble des personnalités de la rive droite à la rive gauche. En début de soirée, et avant le feu d’artifice, l’Armada organise le diner des capitaines.<br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Dimanche 9 Juin<br>
Grand moment d’émotion, fidèle à une tradition des gens de la mer en France : à 10h30, Messe des Marins. Cette messe rassemble entre 2 000 et 5 000 personnes. L’occasion de partager avec tous les marins venus du monde entier. La messe est animée par une chorale de plus de 700 enfants. Et en soirée, feux d’artifice entre 23h et 23h30.<br>

<br><span class="glyphicon glyphicon-arrow-right"> Lundi 10 Juin<br>
La semaine d’animations sur les quais se poursuit. Découvrez le village et les nombreuses possibilités de restauration ou d’activités, sur Seine ou sur les quais. Les voiliers et les navires sont ouverts à la visite. Sur les quais, les animations proposées par la Métropole vous attendent. A découvrir le Panorama XXL, les bars et les restaurants en bord de Seine. Les feux d’artifice vous attendent en soirée à partir de 23h.<br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Mardi 11 Juin<br>
Depuis déjà 7 jours, Rouen est le plus grand port du monde. Les plus beaux voiliers à visiter, les bateaux promenades sur Seine vous permettent de les découvrir avec un autre regard, vue de Seine. Il reste encore 6 jours pour découvrir toutes les animations proposées. Et toujours en soirée, les magnifiques feux d’artifice.<br>

<br><span class="glyphicon glyphicon-arrow-right"> Mercredi 5 Juin<br>
En ouverture et en avant-première de l’Armada, la Seine s’anime … C’est la Grande pagaille. Les équipes doivent construire leur embarcation  et tentent de traverser la Seine sans couler à bord de leur OFNI : Objet Flottant Non Identifié.<br>
Assistez au lancement de la 7e édition de l’Armada en découvrant cette folle traversée riche en émotion, où les équipages rivalisent d’imagination et d’originalité.<br>
En clôture de cette journée, ne manquez pas la levée du Pont Flaubert, le soir entre 21h et minuit.<br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Jeudi 6 Juin<br>
De nouveau le Pont Flaubert se lève entre 5h et 6h du matin. Les voilà !<br>
C’est l’arrivée des navires ! Assistez à cette remontée de la Seine des plus beaux voiliers du monde. Saluez les marins venus de tous les horizons qui débarquent à Rouen, sur les quais, dans la ville.<br>
Pour les plus tardifs, la levée du pont Flaubert est également prévue le soir entre 21h et minuit.<br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Vendredi 7 Juin<br>
Soyez matinaux ! Le Pont Flaubert se lève entre 5h et 6h, une occasion de se promener sur les quais au petit matin ou alors il faut veiller tard car il se lève à nouveau entre 21h et minuit. Cette journée accueille le congrès du Mérite maritime, et en soirée, le magnifique feu d’artifice entre 23h et 23h30.<br>
 
Congrès du Mérite Maritime<br><br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Samedi 8 Juin<br>
Grand jour pour l’Armada ! L’inauguration officielle se déroule à 11h avec l’ensemble des personnalités de la rive droite à la rive gauche. En début de soirée, et avant le feu d’artifice, l’Armada organise le diner des capitaines.<br><br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Dimanche 9 Juin<br>
Grand moment d’émotion, fidèle à une tradition des gens de la mer en France : à 10h30, Messe des Marins. Cette messe rassemble entre 2 000 et 5 000 personnes. L’occasion de partager avec tous les marins venus du monde entier. La messe est animée par une chorale de plus de 700 enfants. Et en soirée, feux d’artifice entre 23h et 23h30.<br><br>
 
<br><span class="glyphicon glyphicon-arrow-right"> Lundi 10 Juin<br>
La semaine d’animations sur les quais se poursuit. Découvrez le village et les nombreuses possibilités de restauration ou d’activités, sur Seine ou sur les quais. Les voiliers et les navires sont ouverts à la visite. Sur les quais, les animations proposées par la Métropole vous attendent. A découvrir le Panorama XXL, les bars et les restaurants en bord de Seine. Les feux d’artifice vous attendent en soirée à partir de 23h.<br><br>

<br><span class="glyphicon glyphicon-arrow-right"> Mardi 11 Juin<br>
Depuis déjà 7 jours, Rouen est le plus grand port du monde. Les plus beaux voiliers à visiter, les bateaux promenades sur Seine vous permettent de les découvrir avec un autre regard, vue de Seine. Il reste encore 6 jours pour découvrir toutes les animations proposées. Et toujours en soirée, les magnifiques feux d’artifice.<br>
            </blockquote>
          </div>

          
          </div></div></div>
        </div>
        
          
           <br>
           <div class="row">
            <div class="col-md-4 col-md-offset-4">
           <h1>L'Armada en image</h1>
           </div>
           </div>
           <div class="row">
             <div class="col-md-12 col-md-offset-1" >
  
        <section class="row">
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau1.png" alt="bateau" class="img-thumbnail""></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau2.png" alt="bateau" class="img-thumbnail" ></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau3.png" alt="bateau" class="img-thumbnail" ></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau4.png" alt="bateau" class="img-thumbnail" ></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau7.png" alt="bateau" class="img-thumbnail" ></div>
       <!--  <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau17.png" alt="bateau" class="img-thumbnail"></div> -->
        
         <!-- <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau15.png" alt="bateau" class="img-thumbnail"></div> -->
       <!--  <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau16.png" alt="bateau" class="img-thumbnail"></div> -->
        
        </section><br>
        <section class="row">
          <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau10.png" alt="bateau" class="img-thumbnail"></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau6.jpg" alt="bateau" class="img-thumbnail" >
        </div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau5.jpg" alt="bateau" class="img-thumbnail"  ></div>
      <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau9.png" alt="bateau" class="img-thumbnail"></div>
       <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau11.png" alt="bateau" class="img-thumbnail"></div>

          
      </section><br>
      <section class="row">
      
      <!-- <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau18.png" alt="bateau" class="img-thumbnail"></div> -->
       
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau7.png" alt="bateau" class="img-thumbnail"></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau8.jpg" alt="bateau" class="img-thumbnail"></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau10.png" alt="bateau" class="img-thumbnail"></div>
  <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau12.png" alt="bateau" class="img-thumbnail"></div>
        <div class="col-xs-4 col-sm-3 col-md-2"><img src="images/bateau13.png" alt="bateau" class="img-thumbnail"></div>
        
        </section>
        </div> <?php } else{?>

    <div class="col-md-4 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span> Attention</div>
                    <div class="panel-body">
                        <p> Vous n'avez pas acces a cette page </p>
                        <?php
                              if( $_SESSION['role'] == "administrateur" ){
                                ?> <a href="admin.php">Accueil</a><?php
                              } 

                              if( $_SESSION['role'] == "Propriétaire de bateaux" ){
                                ?> <a href="proprietaire.php">Accueil</a><?php
                              }

                              if( $_SESSION['role'] != "Propriétaire de bateaux" && $_SESSION['role'] != "administrateur" ){
                                ?> <p><a href="index.php">Accueil</a></p><?php
                              }} ?>

                    </div>
                
                </div>
              </div>


          
    </div>
         
            
    </body>
</html>