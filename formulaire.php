<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Formulaire d'inscription</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="maincss.css">
</head>
<body>
	<header class="container-fluid">
		<div class="row">
			<div class="col-lg-1">
			</div>
			<div class="col-lg-5">
				<img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/Vignette_Armada_Rouen_2019.png" alt="" height="200px" />
			</div>
		</div>
	</header>
	<section>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-5" id="form">
					</br>
					<div class="row justify-content-center">
						<h1>
							Formulaire d'inscription
						</h1>
					</div>
					</br>
					<div class="row justify-content-center">
						<div class="col-lg-8">
							<form class="needs-validation" action="traitementForm.php" method="POST" novalidate>
								<div class="form-group">
								    <label for="lastName">Nom: *</label>
								    <input type="text" name="nom" class="form-control" id="lastName" placeholder="" required>
								    <div class="invalid-feedback">
							         	Veuillez entrer votre nom.
							        </div>
								</div>
								<div class="form-group">
									<label for="firstName">Prénom: *</label>
								    <input type="text" name="prenom" class="form-control" id="firstName" placeholder="" required>
									<div class="invalid-feedback">
							         	Veuillez entrer votre prénom.
							        </div>
								</div>
								<div class="form-group">
									<label for="dateNaiss">Date de naissance: *</label>
								    <input type="date" name="dateNaiss" class="form-control" id="dateNaiss" placeholder="" required>
									<div class="invalid-feedback">
							         	Veuillez entrer votre date de naissance.
							        </div>
							       
                                       
								</div>
								<div class="form-group">
									<label for="eMail">E-mail: *</label>
								    <input type="email" name="eMail" class="form-control" id="eMail" placeholder="" required>
									<div class="invalid-feedback">
							         	Veuillez entrer votre e-mail.
							        </div>
								</div>
								<div class="form-group">
									<label for="mdp">Mot de passe: *</label>
								    <input type="password" name="mdp" class="form-control" id="mdp" placeholder="" required>
									<div class="invalid-feedback">
							         	Veuillez entrer votre mot de passe.
							        </div>
								</div>
								<div class="form-group">
									<label for="confirmMdp">Confirmation mot de passe: *</label>
								    <input type="password" name="confirmMdp" class="form-control" id="confirmMdp" placeholder="" required>
									<div class="invalid-feedback">
							         	Veuillez entrer le même mot de passe.
							        </div>
								</div>
								<br/>
								<div class="row justify-content-center">
									<div class="col-lg-2">
									</div>
									<div class="col">
										<button class="btn btn-primary" type="submit">Confirmer</button>
									</div>
									<div class="col">
										<button class="btn btn-primary" type="reset">Annuler</button>
									</div>
								</div>
								</br>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
	  'use strict';
	  window.addEventListener('load', function() {
	    // Fetch all the forms we want to apply custom Bootstrap validation styles to
	    var forms = document.getElementsByClassName('needs-validation');
	    // Loop over them and prevent submission
	    var validation = Array.prototype.filter.call(forms, function(form) {
	      form.addEventListener('submit', function(event) {
	        if ((form.checkValidity() === false)||(document.querySelector('#mdp').value!==document.querySelector('#confirmMdp').value)) {
	        	document.querySelector('#mdp').value='';
	        	document.querySelector('#confirmMdp').value='';
	          	event.preventDefault();
	          	event.stopPropagation();
	        }
	        form.classList.add('was-validated');
	      }, false);
	    });
	  }, false);
	})();
	</script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>